### WORLD QUIZZ APP
#### TASK LIST




-	Login/Registro de usuario con su cuenta de Google. 	Si es la primera vez estaremos hablando de Registro y a continuación login automático, pero si ya se ha registrado previamente, estaríamos hablando de login.

###### REALIZADO POR VICTOR

- 	Mostrar listado con información principal de países, ordenados alfabéticamente.

###### REALIZADO POR MIGUEL

-	Mostrar localización de los países, geolocalizados en un Mapa de Google integrado en la aplicación.	

###### REALIZADO POR MIGUEL

-	Posibilidad de ver el detalle de un país con imágenes del país tomadas de la API Unplash.

###### REALIZADO POR JUAN ANTONIO

-	Posibilidad de filtrar el listado de país, mostrando los siguientes filtros: por moneda, por idioma. De manera que sólo aparezcan los países que tengan la misma moneda o idioma.

###### REALIZADO POR JUAN ANTONIO

-	El usuario comienza la realización de un test.	50		Una vez que se comienza un test, si se decide abandonar, el usuario recibirá una puntuación de 0.

###### REALIZADO POR VICTOR

- Creación model de preguntas

###### REALIZADO POR MIGUEL

-	El usuario responde a una pregunta de test tipo A

###### REALIZADO POR JUAN ANTONIO

-	El usuario responde a una pregunta de test tipo B

###### REALIZADO POR JUAN ANTONIO

-	El usuario responde a una pregunta de test tipo C

###### REALIZADO POR MIGUEL

-	El usuario responde a una pregunta de test tipo D

###### REALIZADO POR MIGUEL

-	El usuario responde a una pregunta de test tipo E

###### REALIZADO POR JOSÉ MANUEL

-	El usuario responde a una pregunta de test tipo F

###### REALIZADO POR JOSÉ MANUEL

- Comprobaciones para que las respuestas de los test no se repita ninguna respuesta

##### REALIZADO POR JOSÉ MANUEL Y MIGUEL

-	El usuario finaliza el test y obtiene la nota obtenido en el mismo
.		
###### REALIZADO POR VICTOR Y MIGUEL

-	En cualquier momento se podrá consultar el Ranking de usuarios, mostrando el nombre, la foto (si tuviera en su perfil de Google) y el nº de puntos obtenidos y partidas jugadas. Ordenado por puntos obtenidos	4	Terrero	Detalle del Usuario

###### REALIZADO POR JOSÉ MANUEL Y MIGUEL

-	También se podrá consultar el Ranking por efectividad en las respuestas, esto es nº de puntos / nº de partidas jugadas.	

###### REALIZADO POR JOSÉ MANUEL 

- Detalle de usuario en el que se muestra información del usuario

###### REALIZADO POR VÍCTOR





