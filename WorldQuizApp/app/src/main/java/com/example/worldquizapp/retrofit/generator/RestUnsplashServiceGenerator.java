package com.example.worldquizapp.retrofit.generator;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestUnsplashServiceGenerator {

    private static final String BASE_URL = "https://api.unsplash.com";


    private static Retrofit retrofit = null;


    private static HttpLoggingInterceptor logging =
            new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY);

    private static OkHttpClient.Builder httpClient =
            new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    HttpUrl oHttpUrl = original.url();
                    String clientID="2a32e552bc6758a8ca20d4dc6b538cff45a6e0f841aab85e4df6daafbbbed121";

                    HttpUrl url = oHttpUrl.newBuilder()
                            .addQueryParameter("client_id",clientID)
                            .build();

                    Request.Builder requesBuilder = original.newBuilder().url(url);

                    Request request = requesBuilder.build();

                    return chain.proceed(request);
                }
            });


    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build());


    public static <S> S createService(Class<S> serviceClass) {
        if(retrofit==null) {
            httpClient.addInterceptor(logging);
            builder.client(httpClient.build());
            retrofit = builder.build();

        }
        return retrofit.create(serviceClass);
    }

}
