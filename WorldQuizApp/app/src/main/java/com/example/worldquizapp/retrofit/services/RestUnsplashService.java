package com.example.worldquizapp.retrofit.services;


import com.example.worldquizapp.models.ResultadoListaPhotos;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestUnsplashService {

    @GET("/search/photos")
    Call<ResultadoListaPhotos> getImagen(@Query("query") String query);
}
