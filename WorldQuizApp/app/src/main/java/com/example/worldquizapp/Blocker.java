package com.example.worldquizapp;

import android.os.Handler;
import android.view.View;

public abstract class Blocker  implements View.OnClickListener {
    /**
     * This class allows a single click and prevents multiple clicks on
     * the same button in rapid succession. Setting unclickable is not enough
     * because click events may still be queued up.
     *
     * Override onOneClick() to handle single clicks. Call reset() when you want to
     * accept another click.
     */
        private boolean clickable = true;

        /**
         * Override onOneClick() instead.
         */
        @Override
        public final void onClick(View v) {
            if (clickable) {
                clickable = false;
                onOneClick(v);
                //reset(); // uncomment this line to reset automatically
            }
        }

        /**
         * Override this function to handle clicks.
         * reset() must be called after each click for this function to be called
         * again.
         * @param v
         */
        public abstract void onOneClick(View v);

        /**
         * Allows another click.
         */
        public void reset() {
            clickable = true;
        }
    }