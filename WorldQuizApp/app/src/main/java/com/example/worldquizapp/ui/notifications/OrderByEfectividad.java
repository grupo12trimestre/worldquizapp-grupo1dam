package com.example.worldquizapp.ui.notifications;

import com.example.worldquizapp.models.Usuario;

import java.util.Comparator;

public class OrderByEfectividad implements Comparator<Usuario> {

    @Override
    public int compare(Usuario o1, Usuario o2) {
    if (((double)o2.getPuntuacionTotal() / (double)o2.getPartidasJugadas()) - ((double)o1.getPuntuacionTotal() / (double)o1.getPartidasJugadas()) < 0)
        return -1;
    else
        return 1;
    }
}
