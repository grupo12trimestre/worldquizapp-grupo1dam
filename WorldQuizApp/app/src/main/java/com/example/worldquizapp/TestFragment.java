package com.example.worldquizapp;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.worldquizapp.dummy.DummyContent;
import com.example.worldquizapp.dummy.DummyContent.DummyItem;
import com.example.worldquizapp.models.Country;
import com.example.worldquizapp.models.Preguntas;
import com.example.worldquizapp.models.Respuesta;
import com.example.worldquizapp.retrofit.generator.RestCountriesServiceGenerator;
import com.example.worldquizapp.retrofit.services.RestCountriesService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Response;

public class TestFragment extends Fragment {


    private int mColumnCount = 1, puntuacionAnterior;
    private ImageView ivPaisTest;
    private MyTestRecyclerViewAdapter adapter;
    private List<Preguntas> preguntasList;
    private List<Country> paisListAux;
    private List<Country> paisList;
    private Country paisAux;
    private IPreguntasListener mListener;
    private Context ctx;
    private RecyclerView recyclerView;
    private RestCountriesService service;
    DocumentReference docRef;


    public TestFragment() {
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_test_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();

            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            preguntasList = new ArrayList<>();
            service = RestCountriesServiceGenerator.createService(RestCountriesService.class);
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            docRef = db.collection("Usuarios").document(user.getUid());
            docRef.get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                DocumentSnapshot document = task.getResult();
                                if (document.exists()) {
                                    puntuacionAnterior = Integer.parseInt(document.getData().get("puntuacionTotal").toString());
                                    docRef.update("puntuacionAnterior",puntuacionAnterior);
                                }
                            }
                        }
                    });
            new getPreguntas().execute();

        }
        return view;
    }

    private class getPreguntas extends AsyncTask<Void,Void,List<Preguntas>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            paisListAux = new ArrayList<>();
        }

        @Override
        protected List<Preguntas> doInBackground(Void... voids) {

            Call<List<Country>> callPaises = service.listCountries();

            Response<List<Country>> responsePais = null;

            try {
                responsePais = callPaises.execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (responsePais.isSuccessful()) {
                paisList = responsePais.body();
            }

            for (int i = 0; i < 4; i++) {
                Random r = new Random();
                Country p = paisList.get(r.nextInt(paisList.size()-1));
                if (p.getBorders().size() != 0) {
                    paisListAux.add(p);
                }else{
                    i = i-1;
                }

            }

            List<Respuesta> rPstA = new ArrayList<>();

            for(int i = 0;i<4;i++){
                Respuesta rPtA;

                if(i==0){
                    rPtA = new Respuesta(paisListAux.get(i).getCapital(), true);
                    rPstA.add(rPtA);
                }else{
                    rPtA=new Respuesta(paisListAux.get(i).getCapital(), false);
                    rPstA.add(rPtA);
                }
            }

            //Pregunta B
            List<Respuesta> rPstB = new ArrayList<>();

            for (int i =0;i<4;i++){
                Respuesta rPtB;

                if (i==1){
                    rPtB = new Respuesta(paisListAux.get(i).getTranslations().getEs(),true);
                    rPstB.add(rPtB);
                }else {
                    rPtB = new Respuesta(paisListAux.get(i).getTranslations().getEs(), false);
                    rPstB.add(rPtB);
                }
            }


            List<Respuesta> rPstC = new ArrayList<>();

            for (int i = 0; i < 4; i++) {
                Respuesta rPtC;

                if (i == 2) {
                    rPtC = new Respuesta(paisListAux.get(i).getCurrencies().get(0).getName(), true);
                    rPstC.add(rPtC);
                } else {
                    rPtC = new Respuesta(paisListAux.get(i).getCurrencies().get(0).getName(), false);
                    rPstC.add(rPtC);
                }
            }


            List<Respuesta> rPstD = new ArrayList<>();
            for (int i = 0; i < 4; i++) {
                Respuesta rPtD;
                if (i == 3) {
                    rPtD = new Respuesta(paisListAux.get(i).getBorders().get(0), true);
                    rPstD.add(rPtD);
                } else {
                    rPtD = new Respuesta(paisListAux.get(i).getBorders().get(0), false);
                    rPstD.add(rPtD);
                }
            }

            List<Respuesta> rPstE = new ArrayList<>();
            for (int i = 0; i < 4; i++) {
                Respuesta rPtE;
                if (i == 0) {
                    rPtE = new Respuesta(paisListAux.get(i).getLanguages().get(0).getName(), true);
                    rPstE.add(rPtE);
                } else {
                    rPtE = new Respuesta(paisListAux.get(i).getLanguages().get(0).getName(), false);
                    rPstE.add(rPtE);
                }
            }

            Collections.shuffle(rPstA);
            Collections.shuffle(rPstB);
            Collections.shuffle(rPstD);
            Collections.shuffle(rPstC);
            Collections.shuffle(rPstE);

            Preguntas ptA = new Preguntas("¿Cual es la capital de "+paisListAux.get(0).getTranslations().getEs()+" ?",rPstA,"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQP_JCSCnkyjZL440HxxvS486U_bVyiC7piYTYnEtUHKcywQXvm");
            Preguntas ptB = new Preguntas("¿A que pais pertenece esta bandera? ", rPstB,paisListAux.get(1).getAlpha2Code());
            Preguntas ptC = new Preguntas("¿Cúal es la moneda de " + paisListAux.get(2).getTranslations().getEs() + "?", rPstC,"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAz1BMVEX/////pzP/2kTMdADuhwD/3EX/pTL/wjz/qjXdfgD/3kbuhQDtggDJcQDtgAD/2T7/2DX/pCj90j7/1yz/oRf//vr/oiD8zTv5oS32ryf/437/8cH/55D+1UD4uy/6wzX/xHzynhr//fT/4nT/997/7NT/32L/sUv/7rX/7Krjihv3tSv/2Kv/99v/+ebxlxT0pR//3Vf/5oz/6Mv/0ZvtlCPXfg/vjgv/uWD/88v/6Zz/4Gn/xYf/3bb/8eD/umD/16X/xX7/v2//s1D/48DyEKAmAAAN/0lEQVR4nOWdeVviSBPAh0DcTBJYwwQQFRA8UBkF5xXx3Jlx/P6f6e1wSO46uhsyWs+zf26Gn3V1VVd3f/miWxqDk4eDi8fD/aezmxvD/GfHsqzh8PX5+fbl7XTQ0P7va5WTg8fjG2NXyL+BGEIEYSmQHc/zms2m7w3/PL+c/o2c1weHT7vfdpdca1kRrmXHa/r+n98v//1FmCcXx0agNiNFkoQLEZjez9vTbf90hDQeDg2hujS4XMI5ped7R2+DbSPkyeDgeDeHDiJc6LJ5/72gkA0YD0G4MNj7l+JBXv1A4OEIF5CvxfLJ85tvGDw0YQDpD78XJboOHnHqoxEKafrP19uGE3K9v7uLxSMSBop8/W/LfFfHWPNkEQaMf7bpkCdPRD46oVjzbI/xmqo/HmGgx/tt2GrjkMHHIwwYjzaeIM/Tl52aCIMM+WujfCc3lPipglDkDmuD7viDZaCShKWSf7ShJcAD00ClCYWpvm0C8PAbn0+SMFCjdr4rg+uBSghLnm5vvOB7oBpCocZbjXyDMzkFKiEsNYfacuOVTIhRR1jyvP/pATyXCjEKCYWl/tIBuK8CUBFhyX9VzteQd0GVhCVvqDj7XxvyLqiUUDij0nrjCt+m2BShsFSF8eZAiQuqJiz5L6oAlQTRgE78ZyskVBZSL6QBTdO0bdvs1Out1mgvkJIViDyikvWNHKBAM1q9Ubs/qziOK+RrNZByuXx3OZ3u7UhyqkCUABR0re5k7NRc1xFSmcvX8krmqHd30z0ZSnlEtg+adr3bdwK2SkTWhGvQOSUX8ZccIDOKmnarPau5Mbh0wgVl+ZIL6X+XAXzgAC7w4rrLJZSClEkaVwxA2+iOE6aJIJxD3k1LDEZ+6r+mL0Xt+sRNNU4M4RxSKJKOyFzANcjloN3q5+OBhHNF0hmbvJr4jAgo+GoQH0wYQJIZvSEHcJ9mo3Ydw4ciZDB6jHqRlghNow3aJ4FQMF7SYk6TnPlpYdTuOi6KD00o/HFKQqQG1AHFB+36GGWgJEKyqXq0aEOJMvYIz0chDEyVgEiLNo/4KGN2xlgDJRPS1Nh8xgMSnNDu5axfpAkFI8EbCa6IbzvZkxqJj0xIslQP23/7gQWkWiiHMLBUNCEyK6ILCrNFtFAWoRC0M/q4/UWsBu0eMslLE+KdsYmx00NkHLW7lCQhRYhH9I5gwBOkjYosyOBjEuLjjQ9voN7gjNRu8wCZhHhECwI8x9koE9Bxgm6iVkRo7qahVYPurN0b7U3vWIxoxPz16SGKkOeDjjuyTTs4UTLViej9zgPEdWaYQcZt2cudGWtPJ2Ju12YfNardYwHWurax2nviahGVNPJWNqhMYbbIK7VAnJltvBOWrDsOYbmKWsDlZIxjjAo7FU6ir7hdM0RY4imxXMYQevdSKrTHLMBKrWWECXmeWC7fYew0U4kYFdptlo0KHdaVEKKijfczHfAaoUJmlFFHiIs2GUrEBNIOo15SS1iuIrbJ08MpJhfafS6gU4sTshExrpiaEx9hFZrMesJxnUnPiBCW9i7LXFdE2KmX1pVCqLDDy4TubGTYZoywJBZvvAUqKiv6yVIYUVTwEoXjdE1zbQbhaRNrytMjwk6byZ1huC40uxwbrU0MM/yR6DyNdclBxNhpoj+MaJGajMWM4/RsM/KR2MSQtcdRImJpk0gYcAeRk+udWd2MfiU59WUxLBWR9xMdG9gL6xxAIwaYOtfGCDiIYBOLNQcgISMVCsCEpE7uMQoNONg0oyMa8JK0RQ4zTqWDJCzRCWElRiuMgQ4Vuq24iWYSMtZwsBL9AclI6Sp0R3bKd9IJGTU/rMRISgSNlK5CZ5wGmDVBy6j5QSWGa6gGqEJ6IHVb6X+qjAlaup0ilLg20weI0J6QVdhPUaFpG61R+rAlfXED58TQSTc43ZNVuCyWIn+m1mRWC3red6mTJHQlQjoMJX2Iz+xSCZMqNDv9+bxisG9RrV6qUCK4OvVWgGD7gl5UuL1Ypnjf71/szFTLCS+ieyIYa97rYLBwIndInUp8OdqrxaagE4HCogLCseZ9lwZq0JhtcpyZRI3UXKfT9921uBtpiDXv7RpAg4Y9kzVSc/2F9f5h3MgYTWKkI4ItKPp6JhZJzdHazEOz+tPY71GfEpeOCC3ZGEYac8Nw7RzaA44rkaxC0EyX9QW0Z0g30viKLWwEIcKYJ3J2ayBHXOwlQlN6dXrdFM2GYSONEEbNlEEImumiWwNkQ3q6j4fSyJovTBg1MkZXCkz6Pibfc4r7iZn5hRxCRjCFkv68HwUFGpveYtscIeSI81BzARByOlBRK43YeXieJqoBTqSBHNG7hVc0DDdMVr9OaraI/T76si0lp8YJgyoYaHab5NLwfc9+/Y3QrmOEMGymLCMFM6IIpo18G+VtVtQ60ZQfmk+JTH1VQ3OVvB1FKNR4DbDNxunlr8YSQojdeG2xlBUis7sP9vf9wZcTKNCwxp8SBbDdqoTrw7UWL+dHgkusHZoyHGqap1CPxuzx9gw7iXa+MZmrMT6bWC3fXV7ecXdLwVDTfIPSYWTFhRe3nWhEmXav4qZOX/I3vOFVjUiIF0AoJRcWSyUm9mSCQ+tt12HOl2YSAsFUJERg+547mxCv8pdf60y+SigsTYBg6j1DnUTuCFSlFu9FLb/3D3Pznk8ILGnoxeFKiU7KzlPQ1S/tqWXMJ9y5//IEEHIHaFJ3Dxf7FpZSxlzAYFFzlgsolpRcwmB1mhJtFvOle+z0EBdoRGr45SYfkDdCs0LMIuQPmiQJgZRvQa1ERu0URkwm/tXek8UbNCkYoViotWJJI7R/qMYdwVYNRMgbeV4jxneCYzNRMssZVYTcedKV1Pqd7JkoizNpUiwdVhZjUVmE7Kl9hYRyfrhArPXr74yJfXx2YVggwoCxbSSmL9ciZalwLAXyoRJCUU1VuraZRSiFCBPqW9NExHFn8ynFVEJLAhFa01jQutRURBiY6lgwZkwM8Y7QzCUfUKzadNUW6YytrHkavhLB2kJXfZjB2B+l/xL26QS4PtRU42cyfr1Mve6KvUqFCTX1aTLla7Wc3jzSc3jWu9XUa5vL+qK9MGHGfR5MJSJ6bXr6pZV5fhiPZ4lLCb5m/uWZhLmAQb9US887iCmTui2k1Y/9/8uZqKT78DIGlPD9Uz37FsJAV4WhHdudW89ExRl5a/B8wPmgcD4gM12EJqDt6IG+9UxUApFDiNh70rF/GG3qRxNOaGIoZmCsHVLE/qGOPeBYTz9SY4a7+tFfx9rlxuwB69jHj26uRVZ+4UmFqBK17eNvYBZjrGueBjeLAZ3g/vvnafTPRJlbnokqzlwbgxA316ZhNnEWbXVnzSbKRxrcbKL6+dJKbL40/DfKni9lDHoj50uLMiNsaZsR1jHn3S3UnPcnmNX/+OctPv6ZmW2ce0oWwDrPPcFn1+h94YKdXQPPHzJiTdb5w/ZWzh/qOUOaNjFkbukMKeIcMP0Q6axI54D1nOVOjidmEuo/y/3xz+PruVMhbbJtW3cqbPdeDE7ZRL4XQ9fdJtkzUSHZyN0muu6nqYD307BmMTj30+i6Y8gF7xjS0csvpV1K9+HvifoEd31pva+tW4T72j7BnXsf/97Ej3/35Se4v/Tj30H7Ce4R1nsX9KgAd0Fj7/NmRZti3Oet9U52twh3smPv1WctUCP36vM0iHupJP81RL1vIywnaLmAKt5G0P6+xai17fctdL9R4m79jZK//50Z+H25j/9W0Md/74nyZhcn9RfgzS7Cu2t1xgKuCO+uEd7OMzbydh4Sj/KiLPr9Q1OEVKIaC/H+4Sd4w7JA75Du6HmH9BO8JUt8D7iDe7CaSKj3PWDqm87zq2eUEmYdHsoS6pvO8Ph3VOZXz6gkpL7L7ZPf5ea8rY5iLM7b6rRos2REuCOCkMxHjTIraeAfPn5nnLiQP0KE1eollY8eZVaC69pEGTttJ99Y8wlZfEBnJk9oAXUhptkbuznrnBzCavVuSosvS0BETZgl6DIjwmi32rNaFmQWYbVa5qgvAHyBQbLlgIO4gkxVZSrhAo/FV/JTdnspcs5DDCDr3X7FdeMnZRN37gm6uykXTwD+kgMUmZ+LGEDa9V57XKnNORM3Q1bncJfT9HFMLCAj0ytEDChN26z3Ru3+eCYg3Xk3sTpvKAq26V5JAk4VoCziAtMMzgQbnXq9PtoTsmPNRQpOHaCELyZIs19h4QH+UgPIjqipkAoJ5dJEVK52qQu4DRDS66U8uSavUXUTeh57qZYujTP6IlUnoTdE99XQsq/EGRUR+qx6EBIlIVUNobogGpWrf+WdUQWh5ymNMWEZyDujAsLmkFnvouTim6Qa5QlVrWOy5MSQU6MsoWdJlLtIOZRbicsR+vmDForkQSbgSBF6TeT+oLT84HujDKF/pD7LZ8nJDdcb+YTNDXhgWM6Zpsol9OA5GdUyOGSZKo/Q84905sAsuT5mMHIId/x7xXUEWk6eyIx0Qs//s1kHjMoVVY9UQs//uU2+QK73dylxlUbo+a/b5gtk8LiLb3JQCJv+87b8LyHnN1hjRRN6/vD75hI8Qq5+4BSJI/SaxTDPmBwcIyARhALv/qVQ6lvLAIaECL1m8/77NrI7WhoPh8a3HMo8Qs/zvaO3QuMt5fri2BC6TMXMIhSm6d3fFtD3MuX64PFpVygzzpkk3BFw/p/fL/8V1PVy5eTg8fhGqDNQ6JJ1RbjjecLlhOKGP29f/ibVpUhjcPJwcPF4uP90dnMTEFqWNRy+Pj/fvrydDvQr7v9zOYLSSMGHLwAAAABJRU5ErkJggg==");
            Preguntas ptD = new Preguntas("¿Cúales de estos países es frontera de " + paisListAux.get(3).getTranslations().getEs() + "?", rPstD,"https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/El_Hierro_municipio_Frontera.svg/1222px-El_Hierro_municipio_Frontera.svg.png");
            Preguntas ptE = new Preguntas("¿Qué idioma se habla en " + paisListAux.get(0).getTranslations().getEs() + "?", rPstE, "https://image.flaticon.com/icons/png/512/576/576328.png");

            preguntasList.add(ptA);
            preguntasList.add(ptB);
            preguntasList.add(ptC);
            preguntasList.add(ptD);
            preguntasList.add(ptE);

            return preguntasList;
        }

        @Override
        protected void onPostExecute(List<Preguntas> preguntas) {

            super.onPostExecute(preguntas);

            adapter = new MyTestRecyclerViewAdapter(
                    preguntas, ctx,
                    R.layout.fragment_country,
                    mListener

            );

            for (int i =0; i<4;i++) {
                //TODO INCREMENTAR EN UNO EL INDICE DEL GET DEPENDIENDO DEL NUMERO DE PREGUNTAS QUE HAYAS AÑADIDO EXCETO SI ES LA DE TIPO E
                String code = preguntas.get(3).getRespuestas().get(i).getTexto();
                Country c = null;
                try {
                    c = new getPaisPorIsoCode().execute(code).get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //TODO INCREMENTAR EN UNO EL INDICE DEL GET DEPENDIENDO DEL NUMERO DE PREGUNTAS QUE HAYAS AÑADIDO EXCETO SI ES LA DE TIPO E
                preguntas.get(3).getRespuestas().get(i).setTexto(c.getTranslations().getEs());

            }

            recyclerView.setAdapter(adapter);
        }
        }

    private class getPaisPorIsoCode extends AsyncTask<String,Void,Country>{



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
             paisAux = new Country();
        }

        @Override
        protected Country doInBackground(String... countries) {
            Call<Country> callPais = service.paisPorCodigo(countries[0]);
            Response<Country> responsePais = null;
            try {
                responsePais = callPais.execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (responsePais.isSuccessful()) {
                paisAux = responsePais.body();
            }
            return paisAux;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IPreguntasListener) {
            mListener = (IPreguntasListener) context;
            ctx = context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ICountryListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}
