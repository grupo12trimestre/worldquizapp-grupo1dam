package com.example.worldquizapp;

import com.example.worldquizapp.models.Preguntas;

public interface IPreguntasListener {
    void onPreguntaClick(Preguntas p);
}
