package com.example.worldquizapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.example.worldquizapp.models.Constantes;
import com.example.worldquizapp.models.Country;
import com.example.worldquizapp.models.Imagen;
import com.example.worldquizapp.models.ResultadoListaPhotos;
import com.example.worldquizapp.retrofit.generator.RestUnsplashServiceGenerator;
import com.example.worldquizapp.retrofit.services.RestUnsplashService;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.IndicatorView.draw.controller.DrawController;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.xml.transform.Result;

import retrofit2.Call;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;



public class DetallesPaisActivity extends AppCompatActivity {


    TextView tvPais,tvCapital,tvMoneda,tvIdioma,tvRegion,tvPoblacion;
    ResultadoListaPhotos imagenes=null;


    List<String> listaImagenes= new ArrayList<>();
    SliderView sliderView;
    String pais;
    private RestUnsplashService service;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_pais);

        tvPais = findViewById(R.id.textViewPais);
        tvCapital = findViewById(R.id.textViewCapitalPais);
        tvMoneda = findViewById(R.id.textViewMonedaPais);
        tvRegion = findViewById(R.id.textViewPaisGentilicio);
        tvIdioma = findViewById(R.id.textViewIdiomaPais);
        tvPoblacion = findViewById(R.id.textViewPoblacionPais);


        sliderView = findViewById(R.id.imageSlider);

        sliderView.setIndicatorAnimation(IndicatorAnimations.SLIDE); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.CUBEINROTATIONTRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.startAutoCycle();

        sliderView.setOnIndicatorClickListener(new DrawController.ClickListener() {
            @Override
            public void onIndicatorClicked(int position) {
                sliderView.setCurrentPagePosition(position);
            }
        });


        Bundle extras = getIntent().getExtras();

        pais = extras.getString(Constantes.EXTRA_WORLDQUIZAPP_PAIS);
        String capital= extras.getString(Constantes.EXTRA_WORLDQUIZAPP_CAPITAL);
        String moneda= extras.getString(Constantes.EXTRA_WORLDQUIZAPP_MONEDA);
        String poblacion = extras.getString(Constantes.EXTRA_WORLDQUIZAPP_POBLACION);
        String idioma = extras.getString(Constantes.EXTRA_WORLDQUIZAPP_IDIOMA);
        String region= extras.getString(Constantes.EXTRA_WORLDQUIZAPP_REGION);



        tvPais.setText(pais);
        tvCapital.setText(capital);
        tvMoneda.setText(moneda);
        tvIdioma.setText(idioma);
        tvPoblacion.setText(poblacion);
        tvRegion.setText(region);
        service = RestUnsplashServiceGenerator.createService(RestUnsplashService.class);

        new loadImageTask().execute();





    }



    private class loadImageTask extends AsyncTask<String,Void,ResultadoListaPhotos>{

        @Override
        protected ResultadoListaPhotos doInBackground(String... strings) {

            Call<ResultadoListaPhotos> callImg = service.getImagen(pais);


            Response<ResultadoListaPhotos> responseImg = null;


            try {
                responseImg = callImg.execute();

            } catch (IOException e) {
                e.printStackTrace();
            }

            if (responseImg.isSuccessful()) {
                imagenes = responseImg.body();


            }

            List<String> listaAleatoriaA= new ArrayList<>();
            List<String> listaAleatoriaB = new ArrayList<>();

            listaAleatoriaA.add("https://ep01.epimg.net/elpais/imagenes/2018/04/18/opinion/1524063460_922133_1524063996_noticia_normal.jpg");
            listaAleatoriaA.add("https://images4.alphacoders.com/936/936378.jpg");
            listaAleatoriaA.add("https://www.ubackground.com/_ph/12/926928355.jpg");
            listaAleatoriaA.add("https://www.funchap.net/wp-content/uploads/2015/08/eclips-of-sun-space-wallpaper.jpg");
            listaAleatoriaA.add("https://wallpapersmug.com/large/cf7735/lost-in-mind-astronaut.jpg");


            for (int i = 0; i < 5; i++) {
                Random r = new Random();
                String s = listaAleatoriaA.get(r.nextInt(listaAleatoriaA.size()));

                listaAleatoriaB.add(s);
            }

            if (imagenes.getResults().isEmpty()) {

                listaImagenes.add(listaAleatoriaB.get(0));
                listaImagenes.add(listaAleatoriaB.get(1));
                listaImagenes.add(listaAleatoriaA.get(2));


            } else {


                if (imagenes.getResults().size() == 1) {
                    listaImagenes.add(listaAleatoriaB.get(0));
                    listaImagenes.add(listaAleatoriaB.get(1));

                    for (int i = 0; i < 1; i++) {
                        listaImagenes.add(imagenes.getResults().get(i).getUrls().getSmall());
                    }

                } else if (imagenes.getResults().size() == 2) {
                    listaImagenes.add(listaAleatoriaB.get(0));

                    for (int i = 0; i < 2; i++) {
                        listaImagenes.add(imagenes.getResults().get(i).getUrls().getSmall());
                    }

                } else {
                    for (int i = 0; i < 3; i++) {
                        listaImagenes.add(imagenes.getResults().get(i).getUrls().getSmall());
                    }
                }
            }



            return null;
        }

        @Override
        protected void onPostExecute(ResultadoListaPhotos s) {





            SliderAdapter adapter = new SliderAdapter(DetallesPaisActivity.this , listaImagenes);
            adapter.setCount(listaImagenes.size());
            sliderView.setSliderAdapter(adapter);



        }
    }





}
