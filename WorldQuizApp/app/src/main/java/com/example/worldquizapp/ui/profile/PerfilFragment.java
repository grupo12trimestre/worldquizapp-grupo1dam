package com.example.worldquizapp.ui.profile;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.worldquizapp.R;
import com.example.worldquizapp.interfaces.IProfileListener;
import com.example.worldquizapp.models.Country;
import com.example.worldquizapp.models.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.protobuf.StringValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class PerfilFragment extends Fragment {

    public ImageView ivFotoPerfil, ivIconPartidas, ivIconEfectividad, ivIconPuntos,ivIconEmail;
    public TextView tvNombre, tvEmail, tvPuntuacion, tvEfectividad, tvPartidasJugadas;
    FirebaseUser user;
    ProgressBar cargaDatos;
    private IProfileListener mListener;

    public PerfilFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.fragment_perfil, container, false);
        ivFotoPerfil = v.findViewById(R.id.imageViewFotoPerfil);
        tvNombre = v.findViewById(R.id.textViewNombre);
        tvEmail = v.findViewById(R.id.textViewEmail);
        cargaDatos = v.findViewById(R.id.progressBarCarga);
        tvPartidasJugadas = v.findViewById(R.id.textViewPartidasJugadas);
        tvPuntuacion = v.findViewById(R.id.textViewPuntuacion);
        tvEfectividad = v.findViewById(R.id.textViewEfectividad);
        ivIconEfectividad = v.findViewById(R.id.imageViewEfectividad);
        ivIconPuntos = v.findViewById(R.id.imageViewTrofeo);
        ivIconPartidas = v.findViewById(R.id.imageViewPartidas);
        ivIconEmail = v.findViewById(R.id.imageViewEmail);

        user = FirebaseAuth.getInstance().getCurrentUser();

        cargaDatos.setVisibility(View.VISIBLE);
        ivFotoPerfil.setVisibility(View.GONE);
        tvNombre.setVisibility(View.GONE);
        tvEmail.setVisibility(View.GONE);
        tvPuntuacion.setVisibility(View.GONE);
        tvPartidasJugadas.setVisibility(View.GONE);
        tvEfectividad.setVisibility(View.GONE);
        ivIconPuntos.setVisibility(View.GONE);
        ivIconPartidas.setVisibility(View.GONE);
        ivIconEfectividad.setVisibility(View.GONE);
        ivIconEmail.setVisibility(View.GONE);


        user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseFirestore db= FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("Usuarios").document(user.getUid());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        tvPuntuacion.setText(document.getData().get("puntuacionTotal")+" puntos");
                        tvPartidasJugadas.setText(document.getData().get("partidasJugadas")+" partidas");
                        tvEfectividad.setText(document.getData().get("efectividad")+"%");
                    } else {
                        Log.d(TAG, "No such document");
                    }
                    cargaDatos.setVisibility(View.GONE);
                    ivFotoPerfil.setVisibility(View.VISIBLE);
                    tvNombre.setVisibility(View.VISIBLE);
                    tvEmail.setVisibility(View.VISIBLE);
                    tvPuntuacion.setVisibility(View.VISIBLE);
                    tvPartidasJugadas.setVisibility(View.VISIBLE);
                    tvEfectividad.setVisibility(View.VISIBLE);
                    ivIconPuntos.setVisibility(View.VISIBLE);
                    ivIconPartidas.setVisibility(View.VISIBLE);
                    ivIconEfectividad.setVisibility(View.VISIBLE);
                    ivIconEmail.setVisibility(View.VISIBLE);
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });


        if (user != null) {
            String name = user.getDisplayName();
            String email = user.getEmail();
            String photo = String.valueOf(user.getPhotoUrl());
            tvNombre.setText(name);
            tvEmail.setText(email);

            Glide.with(getContext())
                    .load(photo)
                    .error(R.drawable.ic_launcher_foreground)
                    .circleCrop()
                    .into(ivFotoPerfil);

        } else {
            // No user is signed in
        }

        //new AsyncUsuarioIniciado().execute();
        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IProfileListener) {
            mListener = (IProfileListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement IProfileListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
