package com.example.worldquizapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.example.worldquizapp.models.Preguntas;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class TestActivity extends AppCompatActivity implements IPreguntasListener {

    Button btnFinish;
    GoogleSignInClient mGoogleSignInClient;
    FirebaseUser user;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    DocumentReference docRef;
    Fragment f ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        btnFinish = findViewById(R.id.buttonTestTerminado);
        user = FirebaseAuth.getInstance().getCurrentUser();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        docRef = db.collection("Usuarios").document(user.getUid());

         f = new TestFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.frame, f).commit();


        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                docRef
                        .update("partidasJugadas", FieldValue.increment(1));


                DocumentReference docRef = db.collection("Usuarios").document(user.getUid());

                docRef
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if(task.isSuccessful()){
                                    DocumentSnapshot document =task.getResult();
                                    if(document.exists()) {

                                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                                        Log.d(TAG, "DocumentSnapshot data: " + document.getData().get("puntuacionTotal").toString());
                                        DecimalFormat df = new DecimalFormat("###,###,##0.00", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

                                        Double partidas=Double.parseDouble(document.getData().get("partidasJugadas").toString());
                                        Double puntuacionIdeal = partidas*5;

                                        Double efectividad = (100*Double.parseDouble(document.getData().get("puntuacionTotal").toString()))/puntuacionIdeal;
                                        Map<String, Object> updateData = new HashMap<>();
                                        Log.d(TAG, "DocumentSnapshot data: " + efectividad);
                                        updateData.put("efectividad", Double.parseDouble(df.format(efectividad)));
                                        db
                                                .collection("Usuarios")
                                                .document(user.getUid())
                                                .update(updateData);
                                    }
                                    Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                                }else{
                                    Log.d(TAG, "fallo",task.getException());
                                }


                            }
                        });
                Intent i = new Intent(TestActivity.this, DialogTestFinalizado.class);
                DialogFragment dialog = new DialogTestFinalizado();
                dialog.setTargetFragment(f,0);
                dialog.show(f.getFragmentManager(),"DialogTestFinalizado");



            }
        });

    }

    @Override
    public void onPreguntaClick(Preguntas p) {

    }

    //gestionar el salir del test
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            DocumentReference docRef = db.collection("Usuarios").document(user.getUid());
            docRef
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if(task.isSuccessful()){
                                DocumentSnapshot document =task.getResult();
                                if(document.exists()) {

                                    Map<String, Object> updateData = new HashMap<>();
                                    Log.d(TAG, "DocumentSnapshot data: " + document.getData().get("puntuacionTotal"));
                                    updateData.put("puntuacionTotal", Integer.parseInt(document.getData().get("puntuacionAnterior").toString()));
                                    db
                                            .collection("Usuarios")
                                            .document(user.getUid())
                                            .update(updateData);
                                }
                                Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                            }else{
                                Log.d(TAG, "fallo",task.getException());
                            }
                        }
                    });
            Intent i = new Intent(TestActivity.this, MainActivity.class);
            startActivity(i);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}

