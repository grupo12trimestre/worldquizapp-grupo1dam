package com.example.worldquizapp.models;

import java.util.List;

import lombok.Data;

@Data
public class Preguntas {


    private String enunciado;
    private static int puntos;
    private List<Respuesta> respuestas;
    private String url_image;

    public Preguntas(String enunciado, List<Respuesta> respuestas,String url_image) {
        this.enunciado = enunciado;
        this.respuestas = respuestas;
        this.url_image = url_image;
    }
}
