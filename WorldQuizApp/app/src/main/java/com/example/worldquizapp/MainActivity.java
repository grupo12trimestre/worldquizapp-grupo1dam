package com.example.worldquizapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.worldquizapp.interfaces.IProfileListener;
import com.example.worldquizapp.models.Constantes;
import com.example.worldquizapp.models.Country;
import com.example.worldquizapp.models.Currency;
import com.example.worldquizapp.models.Preguntas;

import com.example.worldquizapp.models.Usuario;
import com.example.worldquizapp.ui.notifications.IRankingListener;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;


public class MainActivity extends AppCompatActivity implements  ICountryListener, IRankingListener, IProfileListener,IPreguntasListener, IListarMonedaListener {
    GoogleSignInClient mGoogleSignInClient;
    FirebaseUser user;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_lista,R.id.navigation_mapa, R.id.navigation_ranking, R.id.navigation_profile)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        user = FirebaseAuth.getInstance().getCurrentUser();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this,gso);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,TestActivity.class);
                startActivity(i);
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                finish();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_logout:
                FirebaseAuth.getInstance().signOut();
                mGoogleSignInClient.signOut();
                Intent i = new Intent(this, LoginGoogleActivity.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    @Override
    public void onCountryClick(Country c) {

        Intent i = new Intent(this,DetallesPaisActivity.class);

        i.putExtra(Constantes.EXTRA_WORLDQUIZAPP_PAIS, c.getTranslations().getEs().toUpperCase());
        i.putExtra(Constantes.EXTRA_WORLDQUIZAPP_BANDERA,c.getFlag());
        i.putExtra(Constantes.EXTRA_WORLDQUIZAPP_CAPITAL, c.getCapital());
        i.putExtra(Constantes.EXTRA_WORLDQUIZAPP_REGION, c.getRegion());
        i.putExtra(Constantes.EXTRA_WORLDQUIZAPP_IDIOMA, c.getLanguages().get(0).getName());
        i.putExtra(Constantes.EXTRA_WORLDQUIZAPP_MONEDA, c.getCurrencies().get(0).getName());
        i.putExtra(Constantes.EXTRA_WORLDQUIZAPP_POBLACION, c.getPopulation().toString());
        startActivity(i);
    }
    @Override
    public void listenerProfile() {

    }

    @Override
    public void onUsuarioClick(Usuario u) {

    }


    @Override
    public void onPreguntaClick(Preguntas p) {

    }


    @Override
    public void onMonedaSeleccionada(Currency c) {

    }
}
