package com.example.worldquizapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.worldquizapp.models.Currency;
import com.example.worldquizapp.models.Language;

import java.util.List;

public class LanguageAdapter extends ArrayAdapter<Language> implements IListarIdiomasListener{

    Context ctx;
    int layoutPlantilla;
    List<Language> listaLanguage;


    public LanguageAdapter(@NonNull Context context, int resource, @NonNull List<Language> objects) {
        super(context, resource, objects);
        this.ctx = context;
        layoutPlantilla=resource;
        listaLanguage=objects;
    }

    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        View v = LayoutInflater.from(ctx).inflate(layoutPlantilla, parent, false);

        TextView tvLanguage= (TextView) v.findViewById(android.R.id.text1);

        Language languageActual = listaLanguage.get(position);
        String nombre = languageActual.getName();

        tvLanguage.setText(nombre);


        return v;
    }



    @Override
    public void onClickIdiomaLister(Language l) {

    }
}
