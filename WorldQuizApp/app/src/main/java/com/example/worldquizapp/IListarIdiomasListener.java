package com.example.worldquizapp;

import com.example.worldquizapp.models.Language;

interface IListarIdiomasListener {
    public void onClickIdiomaLister(Language l);
}
