package com.example.worldquizapp;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.example.worldquizapp.models.Preguntas;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class MyTestRecyclerViewAdapter extends RecyclerView.Adapter<MyTestRecyclerViewAdapter.ViewHolder> {
    private final List<Preguntas> mValues;
    private Context ctx;
    private int layout, puntuacionAnterior;
    private IPreguntasListener mListener;
    DocumentReference docRef;


    public MyTestRecyclerViewAdapter(List<Preguntas> objects,Context ctx,int layout, IPreguntasListener listener) {
        mValues = objects;
       this.mListener = listener;
       this.ctx = ctx;
       this.layout = layout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_test, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        docRef = db.collection("Usuarios").document(user.getUid());

        holder.mItem = mValues.get(position);
        holder.tvEnunciado.setText(mValues.get(position).getEnunciado());
        holder.rb1.setText(mValues.get(position).getRespuestas().get(0).getTexto());
        holder.rb2.setText(mValues.get(position).getRespuestas().get(1).getTexto());
        holder.rb3.setText(mValues.get(position).getRespuestas().get(2).getTexto());
        holder.rb4.setText(mValues.get(position).getRespuestas().get(3).getTexto());
        if(position ==1){
            Glide.with(ctx)
                    .load("https://www.countryflags.io/"+mValues.get(position).getUrl_image()+"/flat/64.png")
                    .into(holder.ivFotoTest);

        }else{
            Glide.with(ctx)
                    .load(mValues.get(position).getUrl_image())
                    .into(holder.ivFotoTest);

        }


        if(holder.mItem.getRespuestas().get(0).isCorrecta()) {
            holder.rb1.setOnClickListener(new Blocker() {
                @Override
                public void onOneClick(View v) {
                    docRef
                            .update("puntuacionTotal", FieldValue.increment(1));
                }
            });
        }


        if(holder.mItem.getRespuestas().get(1).isCorrecta()) {
            holder.rb2.setOnClickListener(new Blocker() {
                @Override
                public void onOneClick(View v) {
                    docRef
                            .update("puntuacionTotal", FieldValue.increment(1));
                }
            });
        }

        if(holder.mItem.getRespuestas().get(2).isCorrecta()) {
            holder.rb3.setOnClickListener(new Blocker() {
                @Override
                public void onOneClick(View v) {
                    docRef
                            .update("puntuacionTotal", FieldValue.increment(1));
                }
            });
        }

        if(holder.mItem.getRespuestas().get(3).isCorrecta()) {
            holder.rb4.setOnClickListener(new Blocker() {
                @Override
                public void onOneClick(View v) {
                    docRef
                            .update("puntuacionTotal", FieldValue.increment(1));
                }
            });
        }


            holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
;

            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvEnunciado;
        public final RadioButton rb1;
        public final RadioButton rb2;
        public final RadioButton rb3;
        public final RadioButton rb4;
        public final ImageView ivFotoTest;
        public Preguntas mItem;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvEnunciado = (TextView) view.findViewById(R.id.tvEnunciado);
            rb1 = (RadioButton) view.findViewById(R.id.rb1);
            rb2 = (RadioButton) view.findViewById(R.id.rb2);
            rb3 = (RadioButton) view.findViewById(R.id.rb3);
            rb4 = (RadioButton) view.findViewById(R.id.rb4);
            ivFotoTest =  (ImageView) view.findViewById(R.id.ivFotoTest);
        }


}

}

