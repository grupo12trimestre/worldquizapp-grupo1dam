package com.example.worldquizapp;

import com.example.worldquizapp.models.Country;

public interface ICountryListener {

    void onCountryClick(Country c);
}
