package com.example.worldquizapp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.example.worldquizapp.models.Country;
import com.example.worldquizapp.models.Currency;
import com.example.worldquizapp.retrofit.generator.RestCountriesServiceGenerator;
import com.example.worldquizapp.retrofit.services.RestCountriesService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Response;

public class ListarMonedasDialogFragment extends DialogFragment {


        View v;
        private List<Currency> currencyList = new ArrayList<>();
        private HashMap<String, Currency> currencyMap = new HashMap<>();
        private List<Country> countryList = new ArrayList<>();
        ListView listView;
        private RestCountriesService service;
        private CurrencyAdapter adapter;
        IListarMonedaListener listarMonedaListener;

    public ListarMonedasDialogFragment(IListarMonedaListener listarMonedaListener) {
        this.listarMonedaListener = listarMonedaListener;
    }


    @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Configura el dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            // Configuración del diálogo

            builder.setTitle("Monedas");


            builder.setCancelable(true);

            v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_listar_monedas, null);
            builder.setView(v);

            listView= (ListView) v.findViewById(R.id.listViewIdioma);

            service = RestCountriesServiceGenerator.createService(RestCountriesService.class);
            new getPaisesMoneda().execute();

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Currency item = (Currency) listView.getItemAtPosition(position);


                    listarMonedaListener = (IListarMonedaListener)getTargetFragment();
                    listarMonedaListener.onMonedaSeleccionada(item);

                    getDialog().dismiss();


                }
            });


            builder.setNegativeButton(R.string.button_cancelar, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                }
            });


            // Create the AlertDialog object and return it

            return builder.create();
        }



    private class getPaisesMoneda extends AsyncTask<Void,Void, List<Country>> {

        @Override
        protected void onPreExecute() {



        }

        @Override
        protected List<Country> doInBackground(Void... voids) {

            Call<List<Country>> callPaises = service.listCountries();

            Response<List<Country>> responsePais = null;

            try {
                responsePais = callPaises.execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (responsePais.isSuccessful()) {
                countryList = responsePais.body();

                for (int i=0; i<countryList.size();i++){
                    for(int j=0;j<countryList.get(i).getCurrencies().size();j++) {
                        currencyList.add(countryList.get(i).getCurrencies().get(j));
                    }
                }

                for(Currency c: currencyList){
                    currencyMap.put(c.getName(),c);
                }

                Set<Map.Entry<String,Currency>> set = currencyMap.entrySet();
                currencyList.clear();
                for (Map.Entry<String,Currency> entry:set){
                    currencyList.add(entry.getValue());
                }

                currencyList.remove(0);




            }
            return countryList;
        }

        @Override
        protected void onPostExecute(List<Country> countries) {
            super.onPostExecute(countries);



            adapter = new CurrencyAdapter(getContext(), android.R.layout.simple_list_item_1,currencyList);

            listView.setAdapter(adapter);

        }

    }





    }

