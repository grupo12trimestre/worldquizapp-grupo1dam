package com.example.worldquizapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.worldquizapp.models.Constantes;
import com.example.worldquizapp.models.Country;
import com.example.worldquizapp.retrofit.generator.RestCountriesServiceGenerator;
import com.example.worldquizapp.retrofit.services.RestCountriesService;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Map extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private List<Country> listaCountry;
    private RestCountriesService service;
    private View v;
    private Marker m;


    public Map() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v =inflater.inflate(R.layout.fragment_map, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        service = RestCountriesServiceGenerator.createService(RestCountriesService.class);

        Call<List<Country>> call = service.listCountries();

        call.enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
                if (response.isSuccessful()) {
                    listaCountry = response.body();
                    for (final Country c : listaCountry){
                        if (!c.getLatlng().isEmpty()){
                            Glide.with(getContext())
                                    .asBitmap()
                                    .load("https://www.countryflags.io/"+c.getAlpha2Code()+"/flat/64.png")
                                    .into(new CustomTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                            m = mMap.addMarker(new MarkerOptions()
                                                    .position(new LatLng(c.getLatlng().get(0), c.getLatlng().get(1)))
                                                    .title(c.getTranslations().getEs())
                                                    .icon(BitmapDescriptorFactory.fromBitmap(resource))
                                                    .snippet("Capital: "+c.getCapital())
                                            );
                                            m.setTag(c);
                                        }

                                        @Override
                                        public void onLoadCleared(@Nullable Drawable placeholder) {

                                        }
                                    });
                        }

                    }


                } else {
                    Toast.makeText(v.getContext(), "Error al realizar la petición", Toast.LENGTH_SHORT).show();
                }

                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        Country c = (Country) marker.getTag();
                        Intent i = new Intent(getContext(),
                                DetallesPaisActivity.class);

                        i.putExtra(Constantes.EXTRA_WORLDQUIZAPP_PAIS, c.getTranslations().getEs().toUpperCase());
                        i.putExtra(Constantes.EXTRA_WORLDQUIZAPP_CAPITAL, c.getCapital());
                        i.putExtra(Constantes.EXTRA_WORLDQUIZAPP_REGION, c.getRegion());
                        i.putExtra(Constantes.EXTRA_WORLDQUIZAPP_IDIOMA, c.getLanguages().get(0).getName());
                        i.putExtra(Constantes.EXTRA_WORLDQUIZAPP_MONEDA, c.getCurrencies().get(0).getName());
                        i.putExtra(Constantes.EXTRA_WORLDQUIZAPP_POBLACION, c.getPopulation().toString());
                        startActivity(i);


                        return false;
                    }
                });
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {
                Log.e("Network Failure", t.getMessage());
                Toast.makeText(v.getContext(), "Error al realizar la petición", Toast.LENGTH_SHORT).show();
            }
        });

    }
}