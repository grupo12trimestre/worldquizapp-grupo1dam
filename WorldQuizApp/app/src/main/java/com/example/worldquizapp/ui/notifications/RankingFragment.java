package com.example.worldquizapp.ui.notifications;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.worldquizapp.R;
import com.example.worldquizapp.models.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RankingFragment extends Fragment {

    // TODO: Customize parameters
    private int mColumnCount = 1;
    private IRankingListener mListener;
    private MyRankingRecyclerViewAdapter adapter;
    private List<Usuario> usuarioList ;
    RecyclerView recyclerView;
    private Context ctx;
    private Usuario a;
    private FirebaseFirestore db;
    boolean ordenAsc = true;

    public RankingFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ranking_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            usuarioList = new ArrayList<>();

            db = FirebaseFirestore.getInstance();

            new GetRanking().execute();

        }

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private class GetRanking extends AsyncTask<Void, Void, List<Usuario>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<Usuario> doInBackground(Void... voids) {
            return null;
        }

        @Override
        protected void onPostExecute(final List<Usuario> usuarios) {
            db.collection("Usuarios")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                a = new Usuario();

                                a.setId(document.getId());
                                a.setFotoUrl(document.getData().get("fotoUrl").toString());
                                a.setNombre(document.getData().get("nombre").toString());
                                a.setPuntuacionTotal(Integer.parseInt(document.getData().get("puntuacionTotal").toString()));
                                a.setPartidasJugadas(Integer.parseInt(document.getData().get("partidasJugadas").toString()));
                                a.setEmail( document.getData().get("email").toString());
                                a.setEfectividad(Double.parseDouble(document.getData().get("efectividad").toString()));

                                usuarioList.add(a);
                            }
                        }
                        Toast.makeText(getContext(), "Ordenado alfabéticamente", Toast.LENGTH_SHORT).show();
                        Collections.sort(usuarioList, new OrderByNombre());

                        adapter = new MyRankingRecyclerViewAdapter(usuarioList, ctx, R.layout.fragment_ranking,mListener);
                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                });
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IRankingListener) {
            mListener = (IRankingListener) context;
            ctx = context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement IRankingListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_opciones_ranking, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_order_ranking:
                if(ordenAsc) {
                    item.setIcon(R.drawable.ic_target);

                    Toast.makeText(getContext(), "Ordenado por puntos", Toast.LENGTH_SHORT).show();
                    Collections.sort(usuarioList, new OrderByPuntos());

                    adapter = new MyRankingRecyclerViewAdapter(
                            usuarioList, ctx, R.layout.fragment_ranking,mListener
                    );
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    item.setIcon(R.drawable.ic_grade_white_24dp);

                    Toast.makeText(getContext(), "Ordenado por efectividad", Toast.LENGTH_SHORT).show();
                    Collections.sort(usuarioList, new OrderByEfectividad());

                    adapter = new MyRankingRecyclerViewAdapter(
                            usuarioList, ctx, R.layout.fragment_ranking,mListener
                    );
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
                ordenAsc = !ordenAsc;
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
