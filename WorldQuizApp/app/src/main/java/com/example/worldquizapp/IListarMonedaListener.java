package com.example.worldquizapp;

import com.example.worldquizapp.models.Currency;

interface IListarMonedaListener {
    void onMonedaSeleccionada(Currency c);
}
