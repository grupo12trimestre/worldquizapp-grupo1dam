package com.example.worldquizapp.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Usuario {

    private static Usuario usuario;
    private String id;
    private String fotoUrl;
    private String nombre;
    private int puntuacionTotal;
    private String email;
    private int partidasJugadas;
    private double efectividad;

}
