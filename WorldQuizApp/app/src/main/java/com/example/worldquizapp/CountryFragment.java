package com.example.worldquizapp;

import android.content.Context;
import android.content.Intent;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.view.MenuInflater;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.worldquizapp.models.Country;
import com.example.worldquizapp.models.Currency;
import com.example.worldquizapp.models.Language;
import com.example.worldquizapp.retrofit.generator.RestCountriesServiceGenerator;
import com.example.worldquizapp.retrofit.services.RestCountriesService;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class CountryFragment extends Fragment implements IListarMonedaListener, IListarIdiomasListener {


    private int mColumnCount = 1;
    private ICountryListener mListener;
    private MyCountryRecyclerViewAdapter adapter;
    private List<Country> countryList;
    private Context ctx;
    private RecyclerView recyclerView;
    private RestCountriesService service;
    IListarMonedaListener listarMonedaListener;
    IListarIdiomasListener listarIdiomasListener;
    List<Country> filtroPaisesMoneda;
    List<Country> filtroPaisesIdioma;
    private MenuItem miMoneda;




    public CountryFragment() {
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // ATENCIÓN ESTE FRAGMENT TIENE UN MENÚ ADICIONAL DE OPCIONES
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_country, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.monedaFiltro:
                DialogFragment dialog = new ListarMonedasDialogFragment(listarMonedaListener);
                dialog.setTargetFragment(this,0);
                dialog.show(getFragmentManager(),"ListarMonedasDialogFragment");
                
                

                break;

            case R.id.idiomaFiltro:
                DialogFragment dialogIdioma = new ListarIdiomasDialogFragment(listarIdiomasListener);
                dialogIdioma.setTargetFragment(this,0);
                dialogIdioma.show(getFragmentManager(),"ListarIdiomasDialogFragment");

                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_country_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
         recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            countryList = new ArrayList<>();
            service = RestCountriesServiceGenerator.createService(RestCountriesService.class);

        }

        new getPaises().execute();


        return view;
    }

    @Override
    public void onMonedaSeleccionada(Currency c) {



        filtroPaisesMoneda = new ArrayList<>();

        for (Country country : countryList){
            if(country.getCurrencies()!= null){
                for(Currency currency : country.getCurrencies()){
                    if(currency.getName() != null){
                        if(currency.getName().equals(c.getName())){
                            filtroPaisesMoneda.add(country);
                        }
                    }
                }
            }
        }

        adapter = new MyCountryRecyclerViewAdapter(
                filtroPaisesMoneda, ctx,
                R.layout.fragment_country,
                mListener
        );

        recyclerView.setAdapter(adapter);


    }

    @Override
    public void onClickIdiomaLister(Language l) {

        filtroPaisesIdioma = new ArrayList<>();

        for (Country country : countryList){
            if(country.getLanguages()!= null){
                for(Language language : country.getLanguages()){
                    if(language.getName() != null){
                        if(language.getName().equals(l.getName())){
                            filtroPaisesIdioma.add(country);
                        }
                    }
                }
            }
        }

        adapter = new MyCountryRecyclerViewAdapter(
                filtroPaisesIdioma, ctx,
                R.layout.fragment_country,
                mListener
        );

        recyclerView.setAdapter(adapter);

    }


    private class getPaises extends AsyncTask<Void,Void,List<Country>>{

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected List<Country> doInBackground(Void... voids) {

            Call<List<Country>> callPaises = service.listCountries();

            Response<List<Country>> responsePais = null;

            try {
                responsePais = callPaises.execute();
                } catch (IOException e) {
                e.printStackTrace();
            }

            if (responsePais.isSuccessful()) {
                countryList = responsePais.body();

            }

            return countryList;
        }

        @Override
        protected void onPostExecute(List<Country> countries) {
            super.onPostExecute(countries);

            adapter = new MyCountryRecyclerViewAdapter(
                    countries, ctx,
                    R.layout.fragment_country,
                    mListener
            );


            recyclerView.setAdapter(adapter);


        }

    }





    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ICountryListener) {
            mListener = (ICountryListener) context;
            ctx = context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ICountryListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        listarMonedaListener = null;
    }


}
