package com.example.worldquizapp.models;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Respuesta {

    private String texto;
    private boolean correcta;

    public Respuesta( String texto, boolean correcta) {

        this.texto = texto;
        this.correcta = correcta;
    }
}
