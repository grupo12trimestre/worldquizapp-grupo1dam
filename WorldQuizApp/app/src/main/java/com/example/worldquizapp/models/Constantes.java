package com.example.worldquizapp.models;

public class Constantes {
    public static final String EXTRA_WORLDQUIZAPP_PAIS = "EXTRA_PAIS";
    public static final String EXTRA_WORLDQUIZAPP_IDIOMA = "EXTRA_IDIOMA";
    public static final String EXTRA_WORLDQUIZAPP_CAPITAL = "EXTRA_CAPITAL";
    public static final String EXTRA_WORLDQUIZAPP_MONEDA = "EXTRA_MONEDA";
    public static final String EXTRA_WORLDQUIZAPP_POBLACION = "EXTRA_POBLACION";
    public static final String EXTRA_WORLDQUIZAPP_REGION = "EXTRA_GENTILICIO";
    public static final String EXTRA_WORLDQUIZAPP_MONEDAFILTRO = "EXTRA_MONEDAFILTRO";


    public static final String EXTRA_WORLDQUIZAPP_BANDERA = "EXTRA_BANDERA";
}
