package com.example.worldquizapp.ui.notifications;

import com.example.worldquizapp.models.Usuario;

public interface IRankingListener {
    public void onUsuarioClick(Usuario u);
}
