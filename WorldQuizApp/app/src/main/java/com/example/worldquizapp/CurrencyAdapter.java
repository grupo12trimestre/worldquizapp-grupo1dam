package com.example.worldquizapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.worldquizapp.models.Currency;

import java.util.List;

public class CurrencyAdapter extends ArrayAdapter<Currency> implements IListarMonedaListener{

    Context ctx;
    int layoutPlantilla;
    List<Currency> listaCurrency;


    public CurrencyAdapter(@NonNull Context context, int resource,  @NonNull List<Currency> objects) {
        super(context, resource, objects);
        this.ctx = context;
        layoutPlantilla=resource;
        listaCurrency=objects;
    }

    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        View v = LayoutInflater.from(ctx).inflate(layoutPlantilla, parent, false);

        TextView tvMoneda= (TextView) v.findViewById(android.R.id.text1);

        Currency currencyActual = listaCurrency.get(position);
        String nombre = currencyActual.getName();

        tvMoneda.setText(nombre);


        return v;
    }

    @Override
    public void onMonedaSeleccionada(Currency c) {

    }
}
