package com.example.worldquizapp.ui.notifications;

import com.example.worldquizapp.models.Usuario;

import java.util.Comparator;

public class OrderByNombre implements Comparator<Usuario> {
    @Override
    public int compare(Usuario o1, Usuario o2) {
        return o1.getNombre().compareToIgnoreCase(o2.getNombre());
    }
}
