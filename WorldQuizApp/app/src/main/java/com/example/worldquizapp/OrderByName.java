package com.example.worldquizapp;

import com.example.worldquizapp.models.Country;

import java.util.Comparator;

public class OrderByName implements Comparator<Country> {
    @Override
    public int compare(Country o1, Country o2) {
        return o1.getTranslations().getEs().compareToIgnoreCase(o2.getTranslations().getEs());
    }
}
