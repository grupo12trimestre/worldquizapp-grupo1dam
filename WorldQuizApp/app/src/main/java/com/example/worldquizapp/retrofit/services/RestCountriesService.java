package com.example.worldquizapp.retrofit.services;

import com.example.worldquizapp.models.Country;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RestCountriesService {

    @GET("all")
    Call<List<Country>> listCountries();

    @GET("currency/{code}")
    Call<List<Country>> filtroCurrency(@Path("code") String code);

    @GET("/lang/{lang}")
    Call<List<Country>> filtroLanguague(@Path("lang") String lang);

    @GET("alpha/{code}")
    Call<Country> paisPorCodigo(@Path("code") String code);
}
