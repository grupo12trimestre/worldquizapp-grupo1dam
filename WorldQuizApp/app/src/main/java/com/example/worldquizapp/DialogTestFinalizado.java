package com.example.worldquizapp;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;


public class DialogTestFinalizado extends DialogFragment {

    View v;
    TextView tvPuntuacion;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    Integer puntosPrePartida,puntosPostPartida,puntuacionPartida;
    FirebaseUser user;
    ProgressBar pb;
    public DialogTestFinalizado() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        // Configura el dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Configuración del diálogo
        v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_dialog_test_finalizado, null);

        pb = v.findViewById(R.id.progressBarPartida);
        setCancelable(false);
        user = FirebaseAuth.getInstance().getCurrentUser();

        DocumentReference docRef = db.collection("Usuarios").document(user.getUid());

        pb.setVisibility(View.VISIBLE);
        docRef
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                           @Override
                                           public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                               if (task.isSuccessful()) {
                                                   DocumentSnapshot document = task.getResult();
                                                   if (document.exists()) {
                                                       puntosPrePartida= Integer.parseInt(document.getData().get("puntuacionAnterior").toString());
                                                       puntosPostPartida = Integer.parseInt(document.getData().get("puntuacionTotal").toString());
                                                       if(puntosPostPartida == puntosPrePartida){
                                                           puntuacionPartida = 0;
                                                       }else{
                                                           puntuacionPartida = puntosPostPartida - puntosPrePartida ;
                                                       }
                                                       tvPuntuacion = v.findViewById(R.id.tvPuntuacion);
                                                       String puntuacion = puntuacionPartida.toString();
                                                       tvPuntuacion.setText(puntuacion+"/5");
                                                   }
                                                   pb.setVisibility(View.GONE);
                                               }
                                           }
                                       });


        builder.setCancelable(true);
        builder.setView(v);


        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(getContext(),MainActivity.class);

                dialog.dismiss();
                startActivity(i);
                getActivity().finish();
            }
        });
        return builder.create();

    }
}
