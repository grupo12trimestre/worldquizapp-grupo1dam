package com.example.worldquizapp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.example.worldquizapp.models.Country;
import com.example.worldquizapp.models.Language;
import com.example.worldquizapp.retrofit.generator.RestCountriesServiceGenerator;
import com.example.worldquizapp.retrofit.services.RestCountriesService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Response;

public class ListarIdiomasDialogFragment extends DialogFragment {

    View v;
    private List<Language> languageList = new ArrayList<>();
    private HashMap<String, Language> languageMap = new HashMap<>();
    private List<Country> countryList = new ArrayList<>();
    ListView listView;
    private RestCountriesService service;
    private LanguageAdapter adapter;
    IListarIdiomasListener listarIdiomasListener;

    public ListarIdiomasDialogFragment(IListarIdiomasListener listarIdiomasListener) {
        this.listarIdiomasListener = listarIdiomasListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Configura el dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Configuración del diálogo

        builder.setTitle("Idiomas");


        builder.setCancelable(true);

        v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_listar_idiomas, null);
        builder.setView(v);

        listView= (ListView) v.findViewById(R.id.listViewIdioma);

        service = RestCountriesServiceGenerator.createService(RestCountriesService.class);
        new getPaisesIdioma().execute();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Language item = (Language) listView.getItemAtPosition(position);



                listarIdiomasListener = (IListarIdiomasListener)getTargetFragment();
                listarIdiomasListener.onClickIdiomaLister(item);

                getDialog().dismiss();


            }
        });


        builder.setNegativeButton(R.string.button_cancelar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });


        // Create the AlertDialog object and return it

        return builder.create();
    }

    private class getPaisesIdioma extends AsyncTask<Void,Void, List<Country>> {

        @Override
        protected void onPreExecute() {



        }

        @Override
        protected List<Country> doInBackground(Void... voids) {

            Call<List<Country>> callPaises = service.listCountries();

            Response<List<Country>> responsePais = null;

            try {
                responsePais = callPaises.execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (responsePais.isSuccessful()) {
                countryList = responsePais.body();

                for (int i=0; i<countryList.size();i++){
                    for(int j=0;j<countryList.get(i).getLanguages().size();j++) {
                        languageList.add(countryList.get(i).getLanguages().get(j));
                    }
                }

                for(Language l: languageList){
                    languageMap.put(l.getName(),l);
                }

                Set<Map.Entry<String,Language>> set = languageMap.entrySet();
                languageList.clear();
                for (Map.Entry<String,Language> entry:set){
                    languageList.add(entry.getValue());
                }

                languageList.remove(0);




            }
            return countryList;
        }

        @Override
        protected void onPostExecute(List<Country> countries) {
            super.onPostExecute(countries);



            adapter = new LanguageAdapter(getContext(), android.R.layout.simple_list_item_1,languageList);

            listView.setAdapter(adapter);

        }

    }




}
