package com.example.worldquizapp.ui.notifications;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.worldquizapp.R;
import com.example.worldquizapp.models.Usuario;

import java.util.List;

public class MyRankingRecyclerViewAdapter extends RecyclerView.Adapter<MyRankingRecyclerViewAdapter.ViewHolder> {

    private final List<Usuario> mValues;
    private Context ctx;
    private int layout;
    private IRankingListener mListener;

    public MyRankingRecyclerViewAdapter(List<Usuario> objects, Context ctx, int layout,IRankingListener mListener) {
        mValues = objects;
        this.ctx = ctx;
        this.layout = layout;
        this.mListener = mListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_ranking, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem =  mValues.get(position);
        holder.tvNombre.setText(holder.mItem.getNombre());
        holder.tvPuntos.setText(holder.mItem.getPuntuacionTotal()+"");
        holder.tvPartidas.setText(holder.mItem.getPartidasJugadas()+"");
        holder.tvEfectividad.setText(String.format("%.2f", holder.mItem.getEfectividad()));

/*        if (holder.mItem.getPartidasJugadas() == 0) {
            holder.tvEfectividad.setText("Efectividad: " + 0);
        } else {
            holder.tvEfectividad.setText(String.format("Efectividad: %.2f", ((double) holder.mItem.getPuntuacionTotal() / (double) holder.mItem.getPartidasJugadas())));
        }
*/
        Glide.with(ctx)
                .load(holder.mItem.getFotoUrl())
                .circleCrop()
                .into(holder.ivFoto);

    //  for (int i = 0; i < usuarioList.size(); i++) {
            if (mValues.get(position) == mValues.get(0)) {
                Glide.with(ctx).load(R.drawable.ic_gold_medal).into(holder.ivLogro);
            } else if (mValues.get(position) == mValues.get(1)) {
                Glide.with(ctx).load(R.drawable.ic_silver_medal).into(holder.ivLogro);
            } else if (mValues.get(position) == mValues.get(2)) {
                Glide.with(ctx).load(R.drawable.ic_bronze_medal).into(holder.ivLogro);
            } else {
                holder.ivLogro.setVisibility(View.GONE);
            }
    //   }
    }

    @Override
    public int getItemCount() {

            if (mValues != null) {
                return mValues.size();
            }
            return 0;
        }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvNombre;
        public final TextView tvPuntos;
        public final TextView tvPartidas;
        public final TextView tvEfectividad;
        public final ImageView ivFoto;
        public final ImageView ivLogro;
        public Usuario mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvNombre =  view.findViewById(R.id.textViewRankingNombre);
            tvPuntos = view.findViewById(R.id.textViewRankingPuntos);
            tvPartidas = view.findViewById(R.id.textViewRankingJugadas);
            tvEfectividad = view.findViewById(R.id.textViewRankingEfectividad);
            ivFoto = view.findViewById(R.id.imageViewRankingFoto);
            ivLogro = view.findViewById(R.id.imageViewLogro);
        }
    }
}
