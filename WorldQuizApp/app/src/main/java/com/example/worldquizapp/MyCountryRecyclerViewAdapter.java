package com.example.worldquizapp;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.worldquizapp.models.Country;

import java.util.List;

public class MyCountryRecyclerViewAdapter extends RecyclerView.Adapter<MyCountryRecyclerViewAdapter.ViewHolder> {
    private final List<Country> mValues;
    private Context ctx;
    private int layout;
    private ICountryListener mListener;


    public MyCountryRecyclerViewAdapter(List<Country> objects, Context ctx, int layout, ICountryListener mListener) {
        mValues = objects;
        this.ctx = ctx;
        this.mListener = mListener;
        this.layout = layout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_country, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.tvIdioma.setText(mValues.get(position).getLanguages().get(0).getName());
        if(mValues.get(position).getTranslations().getEs()!=null){
            holder.tvNombre.setText(mValues.get(position).getTranslations().getEs().toUpperCase());
        }

        holder.tvCapital.setText(mValues.get(position).getCapital());

        Glide.with(ctx)
                .load("https://www.countryflags.io/"+ holder.mItem.getAlpha2Code()+"/flat/64.png")
                .into(holder.ivBandera);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onCountryClick(holder.mItem);
            }
        });






    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvNombre;
        public final TextView tvCapital;
        public final TextView tvIdioma;
        public final ImageView ivBandera;
        public Country mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvNombre = (TextView) view.findViewById(R.id.tvNombre);
            tvCapital = (TextView) view.findViewById(R.id.tvCapital);
            tvIdioma = (TextView) view.findViewById(R.id.tvIdioma);
            ivBandera = (ImageView) view.findViewById(R.id.ivFotoTest);
        }


    }
}
